#!/bin/bash
yum update -y
# set timezone
mv -f /etc/localtime /etc/localtime.bak
ln -sf /usr/share/zoneinfo/Europe/Istanbul /etc/localtime

yum -y install pcre-devel.x86_64 compat-libstdc++-33.x86_64 zlib-devel b zip2-devel.x86_64 bzip2-libs.x86_64 lbzip2.x86_64 libzip.x86_64 cyrus-imapd-deve  l.x86_64 mhash-devel.x86_64 libc-client-devel.x86_64 libmcrypt-devel libcurl-devel.x86_64 libtool libjpeg.x86_64 libjpeg-devel.x86_64 freetype-devel.x86_64 freetype.x86_64 curl.x86_64 curl-devel.x86_64 libc-client.x86_64 libc-client-devel.x86_64 zip.x86_64 zlib.x86_64 zlib-devel.x86_64 libxslt.x86_64 libxslt-devel.x86_64 libjpeg-devel.x86_64 libjpeg.x86_64 libpng.x86_64 libpng-devel.x86_64 gd.x86_64 gd-devel.x86_64 gd-progs.x86_64 freetype.x86_64 freetype-devel.x86_64 openssl-devel.x86_64 openssl.x86_64 libtool-ltdl-devel.x86_64 libtool-ltdl.x86_64 libXpm-devel.x86_64 libXpm.x86_64 freetype.x86_64 freetype-devel.x86_64 exim.x86_64 make gcc freetds-devel.x86_64 libc-client-devel ntpdate gcc make automake autoconf unzip lwp openssh-client openssh openssh-clients openssh-server openssh perl-CPAN perl-YAML patch bc curl wget nano xinetd librab bit-mq memcached rsync vim lynx traceroute ntpd mailx

#install php and etc.
yum -y install php54-common php54.x86_64 php54-cli.x86_64 php54-common.x86_64 php54-gd.x86_64 php54-ldap.x86_64 php54-mbstring.x86_64 php54-mcrypt.x86_64 php54-mysql.x86_64 php54-pdo.x86_64
yum -y install php54-pear php54-devel pcre-devel httpd24-httpd-devel
yum -y install php54-pecl-memcached php54-soap git svn telnet 
       

#Apc install
printf "\n" | pecl install apc
echo -e "extension=apc.so \n apc.enabled=1 \n apc.shm_segments=1 \n apc.shm_size=1024M \n apc.ttl=7200 \n apc.user_ttl=3600 \n apc.num_files_hint=128 \n apc.user_entries_hint=100000 \n apc.mmap_file_mask=/tmp/apc.XXXXXX \n apc.enable_cli=1 \n apc.stat=1" > /etc/php.d/apc.ini


#php conf

curl -o /etc/php.ini https://bitbucket.org/listelist/listelist_confs/raw/7a247d41baa912899816d7ae64fcd9bc6d00b9e7/php.ini
#httpd conf
curl -o /etc/httpd/conf/httpd.conf  https://bitbucket.org/listelist/listelist_confs/raw/7a247d41baa912899816d7ae64fcd9bc6d00b9e7/httpd.conf


echo "" > /etc/httpd/conf.d/welcome.conf
curl -o /etc/httpd/conf.d/vhost.conf https://bitbucket.org/listelist/listelist_confs/raw/cf5b4d8840a5419d590b44bf36f761303959db2d/vhost.conf




#bitbucketkey
/bin/cp -rf  /root/serverConf/bitbucket_key ~/.ssh/bitbucket 
chmod 400 ~/.ssh/bitbucket
echo -e "Host bitbucket.org\n	\t IdentityFile ~/.ssh/bitbucket \n\t StrictHostKeyChecking no\n	\t UserKnownHostsFile=/dev/null" > /root/.ssh/config




#vhost Path
mkdir -p /var/www/html/listelist.com


#service httpd
chkconfig httpd on
groupadd www
usermod -a -G www ec2-user
chown -R ec2-user:www /var/www
chmod 2775 /var/www
find /var/www -type d -exec chmod 2775 {} +
find /var/www -type f -exec chmod 0664 {} +
echo "<?php echo 'OKEY'; ?>" > /var/www/html/index.html


#Deployment Progress
#aws s3 sync s3://nnbackup01/codecopy/ /var/www/html/neredennereye.com/
chown -R ec2-user:www /var/www/html/listelist.com/

#service start
/etc/init.d/memcached start
service httpd restart
